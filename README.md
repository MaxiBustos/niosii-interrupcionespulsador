![alt tag](https://raw.githubusercontent.com/bustosperassi/niosii-spi/master/capturas/logo.png)
#Microprocesador NIOS II con módulo PIO (Interrupciones por Pulsador)

##Objetivo
En este proyecto se pretende instanciar un microcontrolador Nios II Fast, que provee Altera mediante un IP CORE, con un módulo de puertos paralelos de entradas y salidas. El Ip-Core (PIO) con interfaz Avalon ofrece una interfaz de memoria mapeada entre el bus Avalon asignado en memoriay un puerto de esclavos de uso general.

##Requisitos
+ Software:
	* Quartus II (version 16.0).
	* Qsys.
	* Nios II EDS.
+ Hardware:
	* Terasic De4

##Clonar Proyecto

	user@host:$ git clone https://bitbucket.org/int_int_team/niosii-pio.git

###Ejecución
Abrir una terminal (ctrl+alt+t) y insertar el siguiente comando

	$ export PATH=/opt/altera/16.0/quartus/sopc_builder/bin:/opt/altera/16.0/nios2eds/sdk2/bin:/opt/altera/16.0/nios2eds/bin:/opt/altera/16.0/nios2eds/bin/gnu/H-x86_64-pc-linux-gnu/bin:$PATH

Se pueden ejecutar distinas reglas en el Make:

Compilar el hardware y software:
	
	$ make

Configurar FPGA tanto software y hardware:

	$ make run

##Desarrollo
Mediante la herramienta llamada Qsys, que provee Quartus II, se puede llegar al siguiente diseño:
  
![alt tag](https://raw.githubusercontent.com/bustosperassi/capturas/master/capturas/NiosII-PIO/qsys-nios-pio.png)

Para ello se necesita cablear e instanciar los siguientes IP-CORES:

1. Nios II.
2. Jtag Uart.
3. Memoria Ram.
4. PIO-Core.
5. Pll (sys_clk).

Con sus respectivas conecxiones:

![alt tag](https://raw.githubusercontent.com/bustosperassi/capturas/master/capturas/NiosII-PIO/Qsysniosii_pio.png)


###Configuraciones de los IP-CORE's 

####Nios II
Nios II es una arquitectura de procesador embebido 32 bits diseñado específicamente para el Altera familia de FPGAs . Nios II incorpora muchas mejoras con respecto a la arquitectura original nios, por lo que es más adecuado para una gama más amplia de aplicaciones informáticas integradas, de DSP para el sistema de control.
Para este proyecto funciona en una velicidad de 125 Mhz y sin MMU. Las configuraciones que se muestran a continuación son las más importantes para este proyecto, ya que las demás quedan por defecto en su uso.	
	
![alt tag](https://raw.githubusercontent.com/bustosperassi/capturas/master/capturas/NiosII-PIO/nios1.png)

	
![alt tag](https://raw.githubusercontent.com/bustosperassi/capturas/master/capturas/NiosII-PIO/nios2.png)

Nota: No se activa la memoria cache.


####JTAG_UART
Este Ip Core se utiliza para poder varias funciones, dentro de las cuales se utiliza para grabar los archivos como el .sof (hardware) y el .elf(software), a su vez poder ver las impresiones por pantallas que nos arroja el microprocesador.
Su configuración es la siguiente:


![alt tag](https://raw.githubusercontent.com/bustosperassi/capturas/master/capturas/NiosII-PIO/jtag.png)


####MEMORY ON CHIP
Se necesta una memoria en las cuales se van a alojar las instruciones y datos del microprocesador:

	
![alt tag](https://raw.githubusercontent.com/bustosperassi/capturas/master/capturas/NiosII-PIO/memory.png)


####PIO
Este IP-Core nos permite manipular los diferentes pulsadores, led's y displays que contiene la placa de desarrollo DE4. Para este proyecto se tomo uno de los cinco posibles pulsadores y se incrementa una variable que muestra su valor en pantalla. Las configuraciones del IP-Core PIO son las siguientes:

![alt tag](https://raw.githubusercontent.com/bustosperassi/capturas/master/capturas/NiosII-PIO/pio_qsys.png)
 

###Sofware 
El software se desarrollo en otra herramienta que nos provee Quartus una vez ya instalado.
Nios II EDS es un entorno de programación en Eclipse ambientado al microprocesador Nios II.

####MainPIO.c
Este archivo contiene el código que se implemento en este trabajo, el donde el microprocesador que en un ciclo infinito a la espera de una interrupción por medio del PIO-Core. 
Se debe inicializar la interrupción como lo indica la siguiente porción de código:

	static void init_button_pio(void)
	{
		/*Recast the edge_capture pointer to match the alt_irq_register() function prototype.*/
		void* edge_capture_ptr = (void*) &edge_capture;
		/*Enable all 4 button interrupts.*/
		IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PIO_0_BASE, 0xf);
		/*Reset the edge capture register.*/
		IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE, 0x0);
		/*Register the interrupt handler.*/
		alt_irq_register( PIO_0_IRQ, edge_capture_ptr,(void *)handle_button_press);
	}

Una vez inicializada la función del botón para interrupciones, por cada vez que se ejecute la interrupción ejecutara la siguiente porción de código (handle de la interrupción):

	static void handle_button_press(void)
	{
		/*Rutina que aumenta la variable global Cuenta en una unidad.*/
		usleep(200);
		cuenta += 1;
		usleep(200);
		printf("Cuenta: %i \n", cuenta);
 		IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE, 0);
	}

###Referencias

Manual Nios II:

 https://www.altera.com/en_US/pdfs/literature/hb/nios2/n2cpu_nii5v1.pdf

Manual IP-Cores:

 https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/ug/ug_embedded_ip.pdf

![alt tag](https://raw.githubusercontent.com/bustosperassi/niosii-spi/master/capturas/logoLCD.jpg)
