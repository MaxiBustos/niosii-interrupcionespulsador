#include <system.h>
#include <unistd.h>
#include <stdio.h>
#include <priv/alt_legacy_irq.h>
#include "sys/alt_stdio.h"
#include <altera_avalon_pio_regs.h>
 
int cuenta = 0;
volatile int edge_capture;

static void handle_button_press(void)
{
	/*Rutina que aumenta la variable global Cuenta en una unidad*/
	usleep(200);
	cuenta += 1;
	usleep(200);
	printf("Cuenta: %i \n", cuenta);
    IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE, 0);
}

static void init_button_pio()
{
    /* Recast the edge_capture pointer to match the alt_irq_register() function prototype. */
    void* edge_capture_ptr = (void*) &edge_capture;
    /* Enable all 4 button interrupts. */
    IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PIO_0_BASE, 0xf);
    /* Reset the edge capture register. */
    IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE, 0x0);
    /* Register the interrupt handler. */
    alt_irq_register( PIO_0_IRQ, edge_capture_ptr,(void *)handle_button_press);
}

int main(void)
{
	alt_printf("Hello Nios II - PIO \n");
	alt_printf("Pusle el boton para aumentar la cuenta\n");
	init_button_pio();
	while(1){}
return 0;
}



