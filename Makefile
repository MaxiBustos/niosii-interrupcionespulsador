#------------------------------------------------------------------------------
#              MAKEFILE PROYECT NIOS II - DDR2 TEST
#------------------------------------------------------------------------------

HDL_PROJECT_NAME=niosii_pio
HDL_PROJECT_PATH=.
HDL_OUTPUT_PATH=$(HDL_PROJECT_PATH)/output_files

SW_PROJECT_PATH=./software
SW_PROJECT_NAME=niosii_pio
SW_OUTPUT_PATH=$(SW_PROJECT_PATH)/$(SW_PROJECT_NAME)


all: hdl sw
	
hdl: qsys
	quartus_cmd $(HDL_PROJECT_PATH)/$(HDL_PROJECT_NAME).qpf -c $(HDL_PROJECT_PATH)/$(HDL_PROJECT_NAME).qsf 

qsys: $(HDL_PROJECT_PATH)/*.qsys
	qsys-generate $(HDL_PROJECT_PATH)/*.qsys --synthesis --clear-output-directory #Soporta solo un qsys por proyecto


sw: sw-bsp sw-app

sw-app:
	$(MAKE) -C $(SW_PROJECT_PATH)/$(SW_PROJECT_NAME)/

sw-bsp:
	mkdir -p $(SW_PROJECT_PATH)/$(SW_PROJECT_NAME)_bsp
	nios2-bsp hal $(SW_PROJECT_PATH)/$(SW_PROJECT_NAME)_bsp
run: run-hw run-sw

run-hw: $(HDL_OUTPUT_PATH)/$(HDL_PROJECT_NAME).sof
	@killall jtagd	#Problema de JTAGD
	@jtagd
	@jtagconfig	#No chequea que se enumere bien
	nios2-configure-sof $(HDL_OUTPUT_PATH)/$(HDL_PROJECT_NAME).sof
	

run-sw: $(SW_OUTPUT_PATH)/$(SW_PROJECT_NAME).elf
	nios2-download -g $(SW_OUTPUT_PATH)/$(SW_PROJECT_NAME).elf
	nios2-terminal

