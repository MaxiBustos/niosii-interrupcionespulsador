module NiosIIpio(
	input          OSC_50_BANK2, // Clock
	input  [1:0] BUTTON			// KEY
);

	wire sys_clk, core_reset_n;

	pll pll (
		.areset	(~BUTTON[0:0]),
		.inclk0	(OSC_50_BANK2),
		.c0		(sys_clk),
		.locked	(core_reset_n)
	);
	
    niosii_pio u0 (
        .clk_clk       (sys_clk),       //   clk.clk
        .pio_o_export  (BUTTON[1:1]),  // pio_o.export
        .reset_reset_n (core_reset_n)  // reset.reset_n
    );
endmodule 