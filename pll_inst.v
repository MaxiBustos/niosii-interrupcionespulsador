
pll pll_inst
(
	.areset(areset_sig) ,	// input  areset_sig
	.inclk0(inclk0_sig) ,	// input  inclk0_sig
	.c0(c0_sig) ,	// output  c0_sig
	.locked(locked_sig) 	// output  locked_sig
);

